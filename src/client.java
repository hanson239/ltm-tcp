import model.Student;

import java.io.DataOutputStream;
import java.net.Socket;

public class client {
  public static void main(String[] args) {

    Socket socket;
    {
      try {
        socket = new Socket("localhost", 11001);
        Student student = new Student();
        student.setMaSV("B16DCCN298");
        student.setName("Hàn Hồng Sơn");
        student.setIP("1");
        student.setGroup(1);

        DataOutputStream out = new DataOutputStream(socket.getOutputStream());
        out.writeUTF(student.getMaSV());
        out.writeUTF(student.getName());
        out.writeUTF(student.getIP());
        out.writeInt(student.getGroup());

      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
}
