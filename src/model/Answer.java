package model;

import java.io.Serializable;
import java.util.Arrays;

public class Answer implements Serializable {
    private final long serialVersionUID = 2L;
    private Student student;
    private Object[] answer;
    private boolean[] isRights;
    private String[] viewString;
    private boolean alreadyRegistration;


    public Answer() {
    }

    @Override
    public String toString() {
        return "model.Answer{" +
                "serialVersionUID=" + serialVersionUID +
                ", student=" + student +
                ", answer=" + Arrays.toString(answer) +
                ", isRights=" + Arrays.toString(isRights) +
                ", viewString=" + Arrays.toString(viewString) +
                ", alreadyRegistration=" + alreadyRegistration +
                '}';
    }

    public long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Object[] getAnswer() {
        return answer;
    }

    public void setAnswer(Object[] answer) {
        this.answer = answer;
    }

    public boolean[] getIsRights() {
        return isRights;
    }

    public void setIsRights(boolean[] isRights) {
        this.isRights = isRights;
    }

    public String[] getViewString() {
        return viewString;
    }

    public void setViewString(String[] viewString) {
        this.viewString = viewString;
    }

    public boolean isAlreadyRegistration() {
        return alreadyRegistration;
    }

    public void setAlreadyRegistration(boolean alreadyRegistration) {
        this.alreadyRegistration = alreadyRegistration;
    }


}
